import feature1.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    static Calculator calculator;
    @BeforeAll
    static void beforeAll() {
        calculator = new Calculator();
    }
    @Test
    void add_ShouldReturnSumOfTwoNumbers() {
        Assertions.assertEquals(20, calculator.add(10, 10));
        Assertions.assertEquals(8, calculator.add(4, 4));
    }
    @Test
    void multi_ShouldReturnMUltipliOfTwoNumbers() {
        Assertions.assertEquals(0, calculator.Multiplying(0, 1));
        Assertions.assertEquals(6, calculator.Multiplying(3, 2));
    }
}